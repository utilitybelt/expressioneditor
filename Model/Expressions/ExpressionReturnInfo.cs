﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleCodeGenApp.Model.Expressions {
    public class ExpressionReturnInfo {
        public Type Type { get; }
        public string Description { get; }
        public string FriendlyType {
            get {
                return Type.ToString().ToLower().Split('.').Last().Split('+').Last().Replace("double", "number");
            }
        }

        public ExpressionReturnInfo(Type type, string description) {
            Type = type;
            Description = description;
        }
    }
}
