﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleCodeGenApp.Model.Compiler;

namespace ExampleCodeGenApp.Model {
    public class NumberLiteral : IStatement {
        public double Value { get; set; }

        public string Compile(CompilerContext context) {
            return Value.ToString();
        }
    }
}
