﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleCodeGenApp.Model.Compiler;
using NodeNetwork.ViewModels;
using System.Diagnostics;

namespace ExampleCodeGenApp.Model {
    public class StatementSequence : IStatement {
        public List<IStatement> Statements { get; } = new List<IStatement>();

        public StatementSequence() { }

        public StatementSequence(IEnumerable<IStatement> connections) {
            Statements.AddRange(connections);
        }

        public string Compile(CompilerContext context) {
            string result = "";
            foreach (IStatement statement in Statements) {
                Debug.WriteLine($"Compile()");
                result += statement.Compile(context);
            }
            return result;
        }
    }
}