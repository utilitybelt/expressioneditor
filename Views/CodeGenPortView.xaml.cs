﻿using System;
using System.Reactive.Disposables;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ExampleCodeGenApp.ViewModels;
using ReactiveUI;

namespace ExampleCodeGenApp.Views {
    public partial class CodeGenPortView : IViewFor<CodeGenPortViewModel> {
        #region ViewModel
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(nameof(ViewModel),
            typeof(CodeGenPortViewModel), typeof(CodeGenPortView), new PropertyMetadata(null));

        public CodeGenPortViewModel ViewModel {
            get => (CodeGenPortViewModel)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel {
            get => ViewModel;
            set => ViewModel = (CodeGenPortViewModel)value;
        }
        #endregion

        #region Template Resource Keys
        public const String ExecutionPortTemplateKey = "ExecutionPortTemplate";
        public const String NumberPortTemplateKey = "NumberPortTemplate";
        public const String StringPortTemplateKey = "StringPortTemplate";
        public const String AnyValuePortTemplateKey = "AnyValuePortTemplate";
        public const String WObjectPortTemplateKey = "WObjectPortTemplate";
        public const String CoordinatesPortTemplateKey = "CoordinatesPortTemplate";
        public const String StopwatchPortTemplateKey = "StopwatchPortTemplate";
        #endregion

        public CodeGenPortView() {
            InitializeComponent();

            this.WhenActivated(d => {
                this.WhenAnyValue(v => v.ViewModel).BindTo(this, v => v.PortView.ViewModel).DisposeWith(d);

                this.OneWayBind(ViewModel, vm => vm.PortType, v => v.PortView.Template, GetTemplateFromPortType)
                    .DisposeWith(d);

                this.OneWayBind(ViewModel, vm => vm.IsMirrored, v => v.PortView.RenderTransform,
                    isMirrored => new ScaleTransform(isMirrored ? -1.0 : 1.0, 1.0))
                    .DisposeWith(d);
            });
        }

        public ControlTemplate GetTemplateFromPortType(PortType type) {
            switch (type) {
                case PortType.Execution: return (ControlTemplate)Resources[ExecutionPortTemplateKey];
                case PortType.Number: return (ControlTemplate)Resources[NumberPortTemplateKey];
                case PortType.String: return (ControlTemplate)Resources[StringPortTemplateKey];
                case PortType.AnyValue: return (ControlTemplate)Resources[AnyValuePortTemplateKey];
                case PortType.WObject: return (ControlTemplate)Resources[WObjectPortTemplateKey];
                case PortType.Coordinates: return (ControlTemplate)Resources[CoordinatesPortTemplateKey];
                case PortType.Stopwatch: return (ControlTemplate)Resources[StopwatchPortTemplateKey];
                default: throw new Exception("Unsupported port type");
            }
        }
    }
}
