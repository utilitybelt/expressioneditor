﻿using ExampleCodeGenApp.Model.Expressions;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UtilityBelt;
using UtilityBelt.Lib;

namespace ExampleCodeGenApp.Lib {
    static class ExpressionLoader {
        public static List<ExpressionInfo> GetExpressions() {
            var expressions = new List<ExpressionInfo>();
            var UB = new UtilityBeltPlugin();
            var toolProps = UB.GetToolProps();

            foreach (var toolProp in toolProps) {
                var nameAttrs = toolProp.PropertyType.GetCustomAttributes(typeof(NameAttribute), true);

                if (nameAttrs.Length == 1) {
                    var name = ((NameAttribute)nameAttrs[0]).Name;
                    expressions.AddRange(GetExpressions(toolProp));
                }
            }

            return expressions;
        }

        private static List<ExpressionInfo> GetExpressions(PropertyInfo toolProp) {
            var expresionInfos = new List<ExpressionInfo>();

            foreach (var method in toolProp.PropertyType.GetMethods()) {
                var expressionMethodAttrs = method.GetCustomAttributes(typeof(ExpressionMethodAttribute), true);

                foreach (var attr in expressionMethodAttrs) {
                    var name = ((ExpressionMethodAttribute)attr).Name;
                    var parameters = GetParameters(method);
                    var summary = GetSummary(method);
                    var usage = GetUsage(method);
                    var examples = GetExamples(method);
                    var returnInfo = GetReturnInfo(method);

                    var expressionInfo = new ExpressionInfo(name, toolProp.Name, summary, parameters, examples, returnInfo);
                    expresionInfos.Add(expressionInfo);
                }
            }

            return expresionInfos;
        }

        private static string GetSummary(MethodInfo methodInfo) {
            var summary = "";
            var attrs = methodInfo.GetCustomAttributes(typeof(SummaryAttribute), true);

            if (attrs.Length == 1) {
                summary = ((SummaryAttribute)attrs[0]).Summary;
            }

            return summary;
        }

        private static string GetUsage(MethodInfo methodInfo) {
            var usage = "";
            var attrs = methodInfo.GetCustomAttributes(typeof(UsageAttribute), true);

            if (attrs.Length == 1) {
                usage = ((UsageAttribute)attrs[0]).Usage;
            }

            return usage;
        }

        private static ExpressionReturnInfo GetReturnInfo(MethodInfo methodInfo) {
            var attrs = methodInfo.GetCustomAttributes(typeof(ExpressionReturnAttribute), true);

            if (attrs.Length == 1) {
                return new ExpressionReturnInfo(((ExpressionReturnAttribute)attrs[0]).Type, ((ExpressionReturnAttribute)attrs[0]).Description);
            }

            return null;
        }

        private static Dictionary<string, string> GetExamples(MethodInfo methodInfo) {
            var examples = new Dictionary<string, string>();
            var attrs = methodInfo.GetCustomAttributes(typeof(ExampleAttribute), true);

            foreach (var attr in attrs) {
                examples.Add(((ExampleAttribute)attr).Command, ((ExampleAttribute)attr).Description);
            }

            return examples;
        }

        private static List<ExpressionParameterInfo> GetParameters(MethodInfo methodInfo) {
            var parameters = new List<ExpressionParameterInfo>();
            var attrs = methodInfo.GetCustomAttributes(typeof(ExpressionParameterAttribute), true);

            foreach (var attr in attrs) {
                var name = ((ExpressionParameterAttribute)attr).Name;
                var type = ((ExpressionParameterAttribute)attr).Type;
                var description = ((ExpressionParameterAttribute)attr).Description;
                parameters.Add(new ExpressionParameterInfo(name, type, description));
            }

            return parameters;
        }
    }
}
