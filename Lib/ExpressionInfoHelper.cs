﻿using ExampleCodeGenApp.Model.Expressions;
using NodeNetwork.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace ExampleCodeGenApp.Lib {
    public class ExpressionInfoHelper : DependencyObject {
        public static readonly DependencyProperty ExpressionInfoProperty =
            DependencyProperty.RegisterAttached("ExpressionInfo", typeof(ExpressionInfo), typeof(NodeView), new PropertyMetadata(null));

        public ExpressionInfo ExpressionInfo {
            get { return (ExpressionInfo)GetValue(ExpressionInfoProperty); }
            set { SetValue(ExpressionInfoProperty, value); }
        }
    }
}
