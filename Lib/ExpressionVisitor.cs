﻿using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UtilityBelt;

namespace ExampleCodeGenApp.Lib {
    public class ExpressionVisitor : MetaExpressionsBaseVisitor<object> {
        /// <summary>
        /// Generic Visitor, this gets called on everything and will convert types to meta expression compatible types
        /// </summary>
        /// <param name="tree"></param>
        /// <returns></returns>
        ///
        public override object Visit([NotNull] IParseTree tree) {
            object v;
            try {
                v = base.Visit(tree);
            }
            catch (NullReferenceException) {
                v = (double)0;
            }

            if (v == null)
                v = (double)0;

            // all bools should be doubles
            if (v.GetType() == typeof(bool))
                v = (double)(((bool)v) ? 1 : 0);

            return v.ToString();
        }


        /// <summary>
        /// Entry parse rule. This will run multiple statements seperated by semicolon
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Returns the result of the last expression</returns>
        public override object VisitParse([NotNull] MetaExpressionsParser.ParseContext context) {
            object lastRet = 0;

            for (var i = 0; i < context.expression().Length; i++)
                lastRet = Visit(context.expression(i));

            return lastRet;
        }

        /// <summary>
        /// Handle numeric atoms
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitNumericAtomExp(MetaExpressionsParser.NumericAtomExpContext context) {
            return context.NUMBER().GetText();
        }

        /// <summary>
        /// Handle string atoms, this includes removing quotes or escaped characters
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>

        public override object VisitStringAtomExp(MetaExpressionsParser.StringAtomExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle bool atoms
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitBoolAtomExp(MetaExpressionsParser.BoolAtomExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle parenthesis
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitParenthesisExp(MetaExpressionsParser.ParenthesisExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle meta expression function calls
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>

        public override object VisitFunctionCall([NotNull] MetaExpressionsParser.FunctionCallContext context) {
            return context.GetText();
        }

        #region Operators
        #region Operator Helpers
        /// <summary>
        /// Checks if an object is "truthy"
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool IsTruthy(object obj) {
            if (obj.GetType() == typeof(double))
                return (double)obj != 0;
            if (obj.GetType() == typeof(string))
                return ((string)obj).Length > 0;

            return true;
        }
        #endregion

        /// <summary>
        /// Handle bbolean comparison operators like && and ||
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitBooleanComparisonExp(MetaExpressionsParser.BooleanComparisonExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle comparison operators like >, >=, ==, etc
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitComparisonExp(MetaExpressionsParser.ComparisonExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle multiplication and division operators
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitMulDivExp(MetaExpressionsParser.MulDivExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle modulo operator
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitModuloExp(MetaExpressionsParser.ModuloExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle addition and subtraction operators
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitAddSubExp(MetaExpressionsParser.AddSubExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle power operation (^)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitPowerExp(MetaExpressionsParser.PowerExpContext context) {
            return context.GetText();
        }

        /// <summary>
        /// Handle regex operation, this handles expr#regex format
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override object VisitRegexExp([NotNull] MetaExpressionsParser.RegexExpContext context) {
            return context.GetText();
        }
        #endregion

        public override object VisitErrorNode([NotNull] IErrorNode node) {
            //Logger.Error($"Some error or something: ({node.Payload}) {node.GetText()}");
            return base.VisitErrorNode(node);
        }
    }
}
