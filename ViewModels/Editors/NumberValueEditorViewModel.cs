﻿using ExampleCodeGenApp.Views.Editors;
using NodeNetwork.Toolkit.ValueNode;
using ReactiveUI;

namespace ExampleCodeGenApp.ViewModels.Editors
{
    public class NumberValueEditorViewModel : ValueEditorViewModel<double?>
    {
        static NumberValueEditorViewModel()
        {
            Splat.Locator.CurrentMutable.Register(() => new NumberValueEditorView(), typeof(IViewFor<NumberValueEditorViewModel>));
        }

        public NumberValueEditorViewModel()
        {
            Value = 0;
        }
    }
}
