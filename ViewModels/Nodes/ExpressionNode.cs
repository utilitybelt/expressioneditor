﻿using DynamicData;
using DynamicData.Binding;
using ExampleCodeGenApp.Model;
using ExampleCodeGenApp.Model.Compiler;
using ExampleCodeGenApp.Model.Expressions;
using ExampleCodeGenApp.Views;
using NodeNetwork;
using NodeNetwork.Toolkit.ValueNode;
using NodeNetwork.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reactive;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ExampleCodeGenApp.ViewModels.Nodes {
    class ExpressionNode : CodeGenNodeViewModel {
        static ExpressionNode() {
            Splat.Locator.CurrentMutable.Register(() => new CodeGenNodeView(), typeof(IViewFor<ExpressionNode>));
        }

        public ExpressionNode(ExpressionInfo expressionInfo) : base(NodeType.Function) {
            ExpressionInfo = expressionInfo;
            Name = ExpressionInfo.MethodName;
            Category = ExpressionInfo.ToolName;

            foreach (var param in ExpressionInfo.Parameters) {
                var paramPortType = PortTypeFromExpressionType(param.FriendlyType);
                var input = new CodeGenInputViewModel<IStatement>(paramPortType, param) {
                    Name = $"{param.Name} ({param.FriendlyType})",
                    MaxConnections = 1,
                    PortPosition = PortPosition.Left,
                    ConnectionValidator = (pending) => {
                        var vm = pending.Output as CodeGenOutputViewModel<IStatement>;
                        var errorMessage = new ErrorMessageViewModel($"Invalid type! {ExpressionInfo.MethodName}[] expects {param.FriendlyType} for input {param.Name}");
                        var isValid = param.Type == typeof(object) || vm.ReturnInfo.FriendlyType == param.FriendlyType;
                        
                        return new ConnectionValidationResult(isValid, isValid ? null : errorMessage);
                    }
                };

                this.Inputs.Add(input);
            }

            List<IObservable<Unit>> inputChanges = new List<IObservable<Unit>>() {
                Changed.Select(_ => Unit.Default).StartWith(Unit.Default)
            };

            foreach (var input in Inputs.Items) {
                inputChanges.Add((input as CodeGenInputViewModel<IStatement>)?.ValueChanged.Select(_ => Unit.Default).StartWith(Unit.Default));
            }

            var returnPortType = PortTypeFromExpressionType(ExpressionInfo.ReturnInfo.FriendlyType);
            Output = new CodeGenOutputViewModel<IStatement>(returnPortType, expressionInfo.ReturnInfo) {
                Name = $"Result ({expressionInfo.ReturnInfo.FriendlyType})",
                PortPosition = PortPosition.Right,
                Value = Observable.CombineLatest(inputChanges).Select(v => {
                    var ec = new ExpressionCall() {
                        FunctionName = ExpressionInfo.MethodName
                    };

                    ec.Parameters.AddRange(ExpressionInfo.Parameters.Select((p, i) => {
                        if (Inputs.Items.Count() > 0 && Inputs.Items.ElementAt(i).Connections.Items.Count() > 0) {
                            return (Inputs.Items.ElementAt(i).Connections.Items.First().Output.Parent as CodeGenNodeViewModel).Output.CurrentValue as IStatement;
                        }
                        else {
                            return new StringLiteral() { Value = "null" };
                        }
                    }));

                    return ec;
                })
            };

            this.Outputs.Add(Output);
        }

        private static PortType PortTypeFromExpressionType(string friendlyType) {
            switch (friendlyType.ToLower()) {
                case "number":
                    return PortType.Number;
                case "string":
                    return PortType.String;
                case "wobject":
                    return PortType.WObject;
                case "coordinates":
                    return PortType.Coordinates;
                case "stopwatch":
                    return PortType.Stopwatch;
                default:
                    return PortType.AnyValue;
            }
        }
    }
}
