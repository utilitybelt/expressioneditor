﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicData;
using ExampleCodeGenApp.Model;
using ExampleCodeGenApp.Model.Compiler;
using ExampleCodeGenApp.Model.Expressions;
using ExampleCodeGenApp.ViewModels.Editors;
using ExampleCodeGenApp.Views;
using NodeNetwork.Toolkit.ValueNode;
using NodeNetwork.ViewModels;
using ReactiveUI;

namespace ExampleCodeGenApp.ViewModels.Nodes {
    public class NumberLiteralNode : CodeGenNodeViewModel {
        static NumberLiteralNode() {
            Splat.Locator.CurrentMutable.Register(() => new CodeGenNodeView(), typeof(IViewFor<NumberLiteralNode>));
        }

        public NumberValueEditorViewModel ValueEditor { get; } = new NumberValueEditorViewModel();

        public NumberLiteralNode() : base(NodeType.Number) {
            this.Name = "Number";
            this.Category = "Literals";

            this.ExpressionInfo = new Model.Expressions.ExpressionInfo("Number", null, "Represents a number", null, null, new ExpressionReturnInfo(typeof(double), "A number"));

            Output = new CodeGenOutputViewModel<IStatement>(PortType.Number, ExpressionInfo.ReturnInfo) {
                Editor = ValueEditor,
                Value = ValueEditor.ValueChanged.Select(v => new NumberLiteral { Value = v ?? 0 }),
                PortPosition = PortPosition.Right
            };
            this.Outputs.Add(Output);
        }
    }
}
