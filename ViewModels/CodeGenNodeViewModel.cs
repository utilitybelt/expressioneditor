﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExampleCodeGenApp.Model.Compiler;
using ExampleCodeGenApp.Views;
using NodeNetwork.Toolkit.ValueNode;
using NodeNetwork.ViewModels;
using ReactiveUI;

namespace ExampleCodeGenApp.ViewModels {
    public enum NodeType {
        ResultNode, Function, FlowControl, Number, Text
    }

    public class CodeGenNodeViewModel : NodeViewModel {
        static CodeGenNodeViewModel() {
            Splat.Locator.CurrentMutable.Register(() => new CodeGenNodeView(), typeof(IViewFor<CodeGenNodeViewModel>));
        }

        public ValueNodeOutputViewModel<IStatement> Output { get; protected set; }

        public NodeType NodeType { get; }

        public string Category { get; set; }

        public Model.Expressions.ExpressionInfo ExpressionInfo { get; set; }

        public CodeGenNodeViewModel(NodeType type) {
            NodeType = type;
        }
    }
}
