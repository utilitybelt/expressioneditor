﻿using NodeNetwork.Toolkit.NodeList;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleCodeGenApp.ViewModels {
    public class CodeGenNodeListViewModel : NodeListViewModel {
        static CodeGenNodeListViewModel() {
            Splat.Locator.CurrentMutable.Register(() => new CodeGenNodeListViewModel(), typeof(IViewFor<CodeGenNodeListViewModel>));
        }

        public CodeGenNodeListViewModel() {
            this.Display = DisplayMode.Tiles;
        }
    }
}
