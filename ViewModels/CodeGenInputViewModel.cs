﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleCodeGenApp.Model.Expressions;
using ExampleCodeGenApp.Views;
using NodeNetwork;
using NodeNetwork.Toolkit.ValueNode;
using NodeNetwork.ViewModels;
using NodeNetwork.Views;
using ReactiveUI;

namespace ExampleCodeGenApp.ViewModels {
    public class CodeGenInputViewModel<T> : ValueNodeInputViewModel<T> {
        static CodeGenInputViewModel() {
            Splat.Locator.CurrentMutable.Register(() => new NodeInputView(), typeof(IViewFor<CodeGenInputViewModel<T>>));
        }

        public ExpressionParameterInfo ParameterInfo { get; }

        public CodeGenInputViewModel(PortType type, ExpressionParameterInfo param) {
            this.Port = new CodeGenPortViewModel { PortType = type };
            this.ParameterInfo = param;

            if (type == PortType.Execution) {
                this.PortPosition = PortPosition.Right;
            }
        }
    }
}
