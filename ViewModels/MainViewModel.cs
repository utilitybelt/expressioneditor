﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DynamicData;
using ExampleCodeGenApp.Lib;
using ExampleCodeGenApp.Model;
using ExampleCodeGenApp.ViewModels.Nodes;
using NodeNetwork.Toolkit.Layout;
using NodeNetwork.Toolkit.Layout.ForceDirected;
using NodeNetwork.Toolkit.NodeList;
using NodeNetwork.ViewModels;
using ReactiveUI;

namespace ExampleCodeGenApp.ViewModels {
    public class MainViewModel : ReactiveObject {
        public NetworkViewModel Network { get; } = new NetworkViewModel();
        public CodeGenNodeListViewModel NodeList { get; } = new CodeGenNodeListViewModel();
        public CodePreviewViewModel CodePreview { get; } = new CodePreviewViewModel();
        public CodeSimViewModel CodeSim { get; } = new CodeSimViewModel();
        public object Import { get; }
        public ReactiveCommand<Unit, Unit> AutoLayout { get; }

        public MainViewModel() {
            ToolTipService.ShowDurationProperty.OverrideMetadata(typeof(DependencyObject), new FrameworkPropertyMetadata(Int32.MaxValue));

            // base result node
            ResultNode resultNode = new ResultNode { CanBeRemovedByUser = false };
            resultNode.Position = new Point(300, 300);
            Network.Nodes.Add(resultNode);

            TextLiteralNode textNode = new TextLiteralNode();
            textNode.Position = new Point(30, 30);
            textNode.ValueEditor.Value = "test";
            Network.Nodes.Add(textNode);

            var con = Network.ConnectionFactory(resultNode.Inputs.Items.First(), textNode.Output);
            Network.Connections.Edit(x => x.Add(con));

            SetupNodeList();

            var codeObservable = resultNode.ResultInputs.Values.Connect().Select(_ => new StatementSequence(resultNode.ResultInputs.Values.Items));
            codeObservable.BindTo(this, vm => vm.CodePreview.Code);
            codeObservable.BindTo(this, vm => vm.CodeSim.Code);

            ForceDirectedLayouter layouter = new ForceDirectedLayouter();
            var config = new Configuration {
                Network = Network,
            };

            AutoLayout = ReactiveCommand.Create(() => layouter.Layout(config, 10000));
        }

        private void SetupNodeList() {
            // literals
            NodeList.AddNodeType(() => new NumberLiteralNode());
            NodeList.AddNodeType(() => new TextLiteralNode());

            // expressions
            LoadExpressions();
        }

        private void LoadExpressions() {
            var expressions = ExpressionLoader.GetExpressions();

            foreach (var expression in expressions) {
                NodeList.AddNodeType(() => new ExpressionNode(expression));
            }
        }
    }
}
